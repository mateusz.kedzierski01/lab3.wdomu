<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" href="Style/styl.css" type="text/css"/>
            <title>Błędne dane</title>
        </head>
        <body>
            <div id="container">
                <h:form>
                    <h2><h:outputText value="Wprowadzono niepoprawne dane!"/></h2>
                    Poprawny zakres elementów macierzy, to: [-99, 99].<br/><br/>
                    <h:commandButton styleClass="button" value="Spróbuj ponownie" action="retry" />
                </h:form>
            </div>
        </body>
     </html>
</f:view>