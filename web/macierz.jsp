<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<f:view>
    <jsp:useBean class="Lab3.historia" id="his" scope="session"/>
    <jsp:useBean class="Lab3.logowanie" id="login" scope="session"/>
    <jsp:useBean class="Lab3.macierz" id="mac" scope="session"/>
 <%
    if(!login.sprawdź()){
        login.setCause("Użytkownik nie jest zalogowany!");
        response.sendRedirect("login_error.jsp");
    }
 %>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" href="Style/styl.css" type="text/css"/>
            <title>Wyznacznik macierzy</title>
        </head>
        <body>
            <jsp:setProperty name="mac" property="el" ></jsp:setProperty>
            <jsp:setProperty name="mac" property="wyznacznik"></jsp:setProperty>
            <div id="container2">
                <h:form>
                    <div class="input">
                        <div class="macierz">
                            Podaj macierz:
                            <table class="mtab">
                                <%--<c:forEach items="#{mac.el}" varStatus="loop">
                                    <c:if test="${loop.index % 3 == 0}"><tr></c:if>
                                        <td><h:inputText styleClass="el_mac" value="#{mac.el[loop.index]}" /></td>
                                    <c:if test="${loop.index % 3 == 2}"></tr></c:if>
                                </c:forEach>--%>
                               <tr>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[0]}" id="e1" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[1]}" id="e2" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[2]}" id="e3" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                </tr>
                                <tr>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[3]}" id="e4" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[4]}" id="e5" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[5]}" id="e6" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                </tr>
                                <tr>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[6]}" id="e7" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[7]}" id="e8" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                    <td><h:inputText autocomplete="false" value="#{mac.el[8]}" id="e9" converterMessage="Podana wartość nie jest liczbą!" validator="#{mac.validate}" styleClass="#{component.valid ? 'el_mac' : 'el_mac invalid'}" /></td>
                                </tr>
                            </table>
                        </div>
                    </div>  
                    <div class="result">
                        Wyznacznik tej macierzy, to:<br/>
                        <h1>
                            <jsp:getProperty name="mac" property="wyznacznik"/>
                        </h1>
                    </div>
                    <div id="controls">
                        <h:messages styleClass="errormsg" layout="table" warnClass="errormsg" infoClass="errormsg" errorClass="errormsg" fatalClass="errormsg"/>
                        <h:commandButton action="#{mac.oblicz}" styleClass="button" value="Oblicz"/>
                        <h:commandButton immediate="true" action="history" styleClass="button" value="Historia wyników"/>
                        <h:commandButton immediate="true" action="#{login.logout}" styleClass="button" value="Wyloguj"/>
                    </div>
                </h:form>
            </div>
        </body>
    </html>
</f:view>