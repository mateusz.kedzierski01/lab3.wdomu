<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>
<f:view>
    <html xmlns:h="http://xmlns.jcp.org/jsf/html" xmlns:f="http://xmlns.jcp.org/jsf/core">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" href="Style/styl.css" type="text/css"/>
            <title>Logowanie</title>
        </head>
        <body>
            <jsp:useBean class="Lab3.logowanie" id="login" scope="session"/>
            <%
                //jeśli zalogowany, to przekieruj do formularza
                if(login.sprawdź()) response.sendRedirect("macierz.jsp");
            %>
            <div id="container">
                <h:form>
                    <h2>Zaloguj się, aby przejść dalej!</h2>
                    <h:outputText value="Nazwa użytkownika" /><br/>
                    <h:inputText autocomplete="false" value="#{login.nazwa}" /><br/><br/>
                    <h:outputText value="Hasło" /><br/>
                    <h:inputSecret autocomplete="false" value="#{login.hasło}" /><br/>
                    <h:commandButton styleClass="button" value="Zaloguj" action="#{login.sprawdź}" />
                </h:form>
            </div>
        </body>
    </html>
</f:view>
