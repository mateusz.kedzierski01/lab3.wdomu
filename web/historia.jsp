<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "f"  uri = "http://java.sun.com/jsf/core"%>
<%@taglib prefix = "h"  uri = "http://java.sun.com/jsf/html"%>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<f:view>
    <jsp:useBean class="Lab3.logowanie" id="login" scope="session"/>
 <%
    if(!login.sprawdź()){
        login.setCause("Użytkownik nie jest zalogowany!");
        response.sendRedirect("login_error.jsp");
    }
 %>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" href="Style/styl.css" type="text/css"/>
            <title>Historia wyników</title>
        </head>
        <body>
            <div id="container2">
                <h:form>
                    <h2>Historia wyników</h2>
                    <jsp:useBean class="Lab3.historia" id="his" scope="session"/>
                        <table id="historia">
                            <tr><th>Data</th><th>Godzina</th><th>Podane wartości</th><th>Wyznacznik</th></tr>
                            <%--dla każdej macierzy w historii--%>
                            <c:forEach items="${his.macierze}" varStatus="i" var="mmm">
                                <tr>
                                    <td><c:out value="${his.getDate(i.index)}"/></td>
                                    <td><c:out value="${his.getTime(i.index)}"/></td>
                                    <td>
                                        <div class="macierz">
                                            <%-- wypisz macierz z historii --%>
                                            <table class="mtab">
                                            <c:forEach begin="0" end="8" varStatus="loop">
                                                <c:if test="${loop.index % 3 == 0}"><tr></c:if>
                                                    <td><input type="text" value="${his.getMacierz(i.index).getElement(loop.index)}" class="el_mac" disabled="true"/></td>
                                                <c:if test="${loop.index % 3 == 2}"></tr></c:if>
                                            </c:forEach>
                                            </table>
                                        </div>
                                    </td>
                                    <td><c:out value="${mmm.wyznacznik}"/></td>
                                </tr>
                            </c:forEach>
                        </table>
                        <%-- jeśli historia pusta --%>
                        <c:if test="${his.size == 0}">
                            <br/>
                            <c:out value="Brak zapisanej historii w tej sesji"/>
                            <br/><br/>
                        </c:if>
                <div id="controls">
                    <h:commandButton immediate="true" action="back" styleClass="button" value="Powrót"/>
                </div>
                </h:form>
            </div>
        </body>
    </html>
</f:view>