<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean class="Lab3.logowanie" id="login"/>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <link rel="stylesheet" href="Style/styl.css" type="text/css"/>
            <title>Błąd logowania</title>
        </head>
        <body>
            <div id="container">
                <h:form id="formularz_error">
                    <h2><h:outputText value="Błąd logowania"/></h2>
                    <h:outputText value="#{login.cause}" /><br/><br/>
                    <h:commandButton styleClass="button" value="Spróbuj ponownie" action="retry" />
                </h:form>
            </div>
        </body>
     </html>
</f:view>