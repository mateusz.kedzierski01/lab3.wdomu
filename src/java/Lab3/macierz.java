/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab3;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mefju
 */
public class macierz implements Serializable {

    private int e1, e2, e3, e4, e5, e6, e7, e8, e9;
    private Integer[] el;
    private Integer wyznacznik;

    public Integer getWyznacznik() {
        return wyznacznik;
    }

    public void setWyznacznik(Integer wyznacznik) {
        this.wyznacznik = wyznacznik;
    }

    public int getE1() {return e1;}
    public int getE2() {return e2;}
    public int getE3() {return e3;}
    public int getE4() {return e4;}
    public int getE5() {return e5;}
    public int getE6() {return e6;}
    public int getE7() {return e7;}
    public int getE8() {return e8;}
    public int getE9() {return e9;}
    
    public void setE1(int e1) {this.e1 = e1;}
    public void setE2(int e2) {this.e2 = e2;}
    public void setE3(int e3) {this.e3 = e3;}
    public void setE4(int e4) {this.e4 = e4;}
    public void setE5(int e5) {this.e5 = e5;}
    public void setE6(int e6) {this.e6 = e6;}
    public void setE7(int e7) {this.e7 = e7;}
    public void setE8(int e8) {this.e8 = e8;}
    public void setE9(int e9) {this.e9 = e9;}

    public Integer[] getEl() {
        return el;
    }

    public String getElement(int index){
        return el[index].toString();
    }
    
    public void setEl(Integer[] el) {
        this.el = el;
    }
    
    
    public String oblicz(){
        //obliczenie wyznacznika metodą Sarrusa
        this.wyznacznik =  (el[0] * el[4] * el[8] + 
                            el[1] * el[5] * el[6] + 
                            el[2] * el[3] * el[7])
                            - 
                           (el[2] * el[4] * el[6] + 
                            el[0] * el[5] * el[7] + 
                            el[1] * el[3] * el[8]);
        macierz m = new macierz();
        m.setEl(this.el.clone());
        m.setWyznacznik(this.wyznacznik);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        historia h = (historia) session.getAttribute("his");
        h.dodajMacierz(m);
        return "result";
    }
    
    
    public macierz() {
        this.wyznacznik = 0;
        el = new Integer[9];
    }
    
    
    // walidacja wprowadzonych danych
    // jeśli użytkownik poda znak inny niż liczba, to walidację wykonuje converter
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        FacesMessage m = new FacesMessage();
        m.setSeverity(FacesMessage.SEVERITY_WARN);
        List<FacesMessage> list = context.getMessageList();
        int x;
        try{
            x = Integer.parseInt(value.toString());
            
            //za mała wartość
            if(x<-99) {
                m.setSummary("Wprowadzona liczba jest za mała!");
                if(list.isEmpty()) throw new ValidatorException(m);
                else throw new ValidatorException(new FacesMessage(null, null));
            }
            
            //za duża wartość
            if(x>99) {
                m.setSummary("Wprowadzona liczba jest za duża!");
                // zabezpieczenie przed dublowaniem msg
                for(FacesMessage fm : list)
                    if(fm.getSummary().equals("Wprowadzona liczba jest za mała!")) list.remove(fm);
                if(list.isEmpty()) throw new ValidatorException(m);
                else throw new ValidatorException(new FacesMessage(null, null));
            }
            
        //brak podanej wartości
        } catch (NullPointerException np){
            m.setSummary("Nie podano wszystkich wartości!");
            // zabezpieczenie przed dublowaniem msg
            for(FacesMessage fm : list)
                if(fm.getSummary().equals("Wprowadzona liczba jest za mała!")) list.remove(fm);
            if(list.isEmpty()) throw new ValidatorException(m);
            else throw new ValidatorException(new FacesMessage(null, null));
        }
    
        
}
    
}
