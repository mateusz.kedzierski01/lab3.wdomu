package Lab3;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
public class historia implements Serializable {

    // beany wprowadzonych macierzy (sklonowane)
    private ArrayList<macierz> macierze;
    // czasy wykonania działania
    private ArrayList<Date> daty;
    
    public historia() {
        macierze = new ArrayList<>();
        daty = new ArrayList<>();
    }

    public ArrayList<macierz> getMacierze() {
        return macierze;
    }

    public void setMacierze(ArrayList<macierz> macierze) {
        this.macierze = macierze;
    }

    public ArrayList<Date> getDaty() {
        return daty;
    }

    public void setDaty(ArrayList<Date> daty) {
        this.daty = daty;
    }
    
    // sama data
    public String getDate(int index){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(daty.get(index));
    }
    
    //sam czas
    public String getTime(int index){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(daty.get(index));
    }
    
    public macierz getMacierz(int index){
        return macierze.get(index);
    }
    
    public void dodajMacierz(macierz m){
        daty.add(new Date(System.currentTimeMillis()));
        macierze.add(m);
    }
    
    public int getSize(){
        return this.macierze.size();
    }
    
}
