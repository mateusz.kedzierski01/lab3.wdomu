package Lab3;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class logowanie {

    private String nazwa;
    private String hasło;
    private String cause;
    private boolean logged = false;
    

    public String getNazwa() {
        return nazwa;
        }
    
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
        }
    
    public String getHasło() {
        return hasło;
    }
     
    public void setHasło(String hasło) {
        this.hasło = hasło;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
    
    //sprawdź poprawność danych logowania, ustaw powód błędu
    public Boolean sprawdź(){
        if(nazwa==null || hasło==null){
            cause = "Użytkownik niezalogowany!";
            return false;
        }
        boolean user, pass;
        user = nazwa.equals("student");
        pass = hasło.equals("wcy");
        if(!user || !pass) cause = "Niepoprawne dane logowania!";
        return user && pass;
    }

    public String logout(){
        // wyczyść properties
        nazwa = null;
        hasło = null;
        cause = "";
        logged = false;
        
        //zakończ sesję
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.invalidate();
        return "logout";
    }
    
    public logowanie() {
    
    }
    
}
